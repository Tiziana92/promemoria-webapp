import React from 'react';
import logo from './logo.svg';
import './App.css';
import {List} from "./Models/List";
import {Promemoria} from "./Models/Promemoria";

function App() {
    /*
    var list1 = new List ("lista1",1,2);
    var promemoria1= new Promemoria("casa","cucina");

    console.log(list1)
    console.log(promemoria1)*/

    ReactDOM.render(
        <h1>Hello, world!</h1>,
        document.getElementById('root')
    );
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
