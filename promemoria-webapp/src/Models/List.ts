export class List {
    name: string;
    promemoria: number;
    priorità: number;

    constructor(myname: string, promemoria:number, priorità:number) {
        this.name = myname;
        this.promemoria = promemoria;
        this.priorità = priorità;
    }

}
